$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'elite/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, hidden: true },
			{ label: '姓名', name: 'name', index: 'name', width: 80 }, 			
			{ label: '经销商', name: 'merchant', index: 'merchant', width: 80 }, 			
			{ label: '大区名称', name: 'areaName', index: 'area_name', width: 80 }, 			
			{ label: '个人简介', name: 'personProfile', index: 'person_profile', width: 80 }, 			
			{ label: '票数', name: 'votes', index: 'votes', width: 80 },		
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
    
    $.ajax({
		type: "POST",
	    url: baseURL + 'area/list',
        contentType: "application/json",
	    success: function(r){
	    	if(r.code === 0){
				$.each(r.page.list,function(i,n){
					$("#areaName").append("<option value='"+n.id+"'>"+n.name+"</option>");
				});
			}else{
				alert(r.msg);
			}
		}
	});
    
    $("#startTime").datetimepicker({
        format: 'yyyy-mm-dd',
        minView:'month',
        minuteStep:1,
        language: 'zh-CN',
        autoclose:true,
        //startDate:new Date(),
        clearBtn: true 
    });
    $("#endTime").datetimepicker({
    	format: 'yyyy-mm-dd',
        minView:'month',
        minuteStep:1,
        language: 'zh-CN',
        autoclose:true,
        //startDate:new Date(),
        clearBtn: true 
    });
    
    new AjaxUpload('#upload', {
        action: baseURL + 'sys/oss/upload',
        name: 'file',
        autoSubmit:true,
        responseType:"json",
        onSubmit:function(file, extension){
            if(vm.config.type == null){
                alert("云存储配置未配置");
                return false;
            }
            if (!(extension && /^(jpg|jpeg|png|gif)$/.test(extension.toLowerCase()))){
                alert('只支持jpg、png、gif格式的图片！');
                return false;
            }
        },
        onComplete : function(file, r){
            if(r.code == 0){
            	vm.elite.head = r.url;
                $("#head").val(r.url);
                $("#uploadImage").attr("src",r.url);
            }else{
                alert(r.msg);
            }
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		elite: {}
	},
	created: function(){
        this.getConfig();
    },
	methods: {
		query: function () {
			vm.reload();
		},
		getConfig: function () {
            $.getJSON(baseURL + "sys/oss/config", function(r){
				vm.config = r.config;
            });
        },
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.elite = {};
			$("#uploadImage").attr("src", "");
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		exportExcel: function (event) {
			window.location.href=baseURL + "elite/export";
		},
		saveOrUpdate: function (event) {
			var url = vm.elite.id == null ? "elite/save" : "elite/update";
			vm.elite.areaName = $("#areaName").find("option:selected").text();
			vm.elite.startTime = $("#startTime").val()+" 00:00:00";
			vm.elite.endTime = $("#endTime").val()+" 23:59:59";
			$.ajax({
				type: "POST",
			    url: baseURL + url, 
                contentType: "application/json",
			    data: JSON.stringify(vm.elite),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "elite/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "elite/info/"+id, function(r){
                vm.elite = r.elite;
                $("#uploadImage").attr("src",r.elite.head);
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});