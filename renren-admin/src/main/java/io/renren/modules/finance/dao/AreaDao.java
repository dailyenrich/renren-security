package io.renren.modules.finance.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.modules.finance.entity.AreaEntity;

/**
 * 
 * 
 * @date 2018-07-30 09:51:16
 */
public interface AreaDao extends BaseMapper<AreaEntity> {
	
}
