package io.renren.modules.finance.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import com.github.crab2died.annotation.ExcelField;

/**
 * 
 * 
 * @date 2018-07-30 09:51:16
 */
@TableName("elite")
public class EliteEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 姓名
	 */
	@ExcelField(title = "姓名", order = 1)
	private String name;
	
	/**
	 * 头像
	 */
	private String head;
	/**
	 * 经销商
	 */
	@ExcelField(title = "经销商", order = 2)
	private String merchant;
	/**
	 * 大区ID
	 */
	private Integer areaId;
	/**
	 * 大区名称
	 */
	@ExcelField(title = "大区名称", order = 3)
	private String areaName;
	/**
	 * 个人简介
	 */
	@ExcelField(title = "个人简介", order = 4)
	private String personProfile;
	/**
	 * 票数
	 */
	@ExcelField(title = "票数", order = 5)
	private String votes;
	/**
	 * 投票开始时间
	 */
	private Date startTime;
	
	/**
	 * 投票结束时间
	 */
	private Date endTime;
	/**
	 * 备注
	 */
	private String remark;
	
	@Version
	private Integer version;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：姓名
	 */
	public String getName() {
		return name;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	/**
	 * 设置：经销商
	 */
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	/**
	 * 获取：经销商
	 */
	public String getMerchant() {
		return merchant;
	}
	/**
	 * 设置：大区ID
	 */
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	/**
	 * 获取：大区ID
	 */
	public Integer getAreaId() {
		return areaId;
	}
	/**
	 * 设置：大区名称
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	/**
	 * 获取：大区名称
	 */
	public String getAreaName() {
		return areaName;
	}
	/**
	 * 设置：个人简介
	 */
	public void setPersonProfile(String personProfile) {
		this.personProfile = personProfile;
	}
	/**
	 * 获取：个人简介
	 */
	public String getPersonProfile() {
		return personProfile;
	}
	/**
	 * 设置：票数
	 */
	public void setVotes(String votes) {
		this.votes = votes;
	}
	/**
	 * 获取：票数
	 */
	public String getVotes() {
		return votes;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
}
