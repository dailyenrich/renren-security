package io.renren.modules.finance.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 
 * 
 * @date 2018-07-30 09:51:16
 */
@TableName("area")
public class AreaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 大区名称
	 */
	private String name;
	/**
	 * 大区备注
	 */
	private String remark;

	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：大区名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：大区名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：大区备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：大区备注
	 */
	public String getRemark() {
		return remark;
	}
}
