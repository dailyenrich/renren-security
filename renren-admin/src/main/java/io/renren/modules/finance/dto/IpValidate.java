package io.renren.modules.finance.dto;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.renren.common.utils.DateUtils;

public class IpValidate {
	//投票
	public static Map<String,String> VALIDATE = new ConcurrentHashMap<String,String>(50000);
	
	public static Map<String,Map<String,Integer>> VOTES_TEMP = new ConcurrentHashMap<String,Map<String,Integer>>(); 
	
	public static Map<String,Integer> VOTES = new ConcurrentHashMap<String,Integer>();
	
	public static Boolean validateIp(String ip) {
		
		String dd = DateUtils.format(new Date());
		
		Map<String,Integer> vote = null == VOTES_TEMP.get(dd) ? new ConcurrentHashMap<String,Integer>(50000) : VOTES_TEMP.get(dd);
		if (!vote.containsKey(dd+ip)) {
			vote.put(dd+ip, 1);
			VOTES_TEMP.put(dd, vote);
			return true;
		}
		return false;
	}
	
	public static int getSeconds() {
		Date endDate = DateUtils.stringToDate(DateUtils.format(new Date(), DateUtils.DATE_PATTERN)+" 23:59:59",
				DateUtils.DATE_TIME_PATTERN);
		Date startDate = new Date();
	    Long interval = (endDate.getTime() - startDate.getTime())/1000;
		return interval.intValue();
	}
}
