package io.renren.modules.finance.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.modules.finance.dto.VoteDto;
import io.renren.modules.finance.entity.AreaEntity;
import io.renren.modules.finance.entity.EliteEntity;

/**
 * 
 * 
 * @date 2018-07-30 09:51:16
 */
public interface EliteDao extends BaseMapper<EliteEntity> {
	
	int updateEliteVote(VoteDto voteDto);
	
	List<AreaEntity> queryArea();
	
	List<EliteEntity> queryElite(Map<String,Object> params);
}
