package io.renren.modules.finance.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.code.kaptcha.Producer;

import io.renren.common.annotation.RateLimit;
import io.renren.common.utils.R;
import io.renren.common.utils.RedisUtils;
import io.renren.modules.finance.dto.IPUtils;
import io.renren.modules.finance.dto.IpValidate;
import io.renren.modules.finance.dto.VoteDto;
import io.renren.modules.finance.entity.AreaEntity;
import io.renren.modules.finance.entity.EliteEntity;
import io.renren.modules.finance.service.EliteService;



/**
 * 
 *
 * @date 2018-07-30 09:51:16
 */
@RestController
@RequestMapping("vote")
public class VoteController {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
    @Autowired
    private EliteService eliteService;
    
    @Autowired
	private Producer producer;
    
    private static List<AreaEntity> aes = null;
    
    @Autowired
    RedisUtils redisUt;
    
    @PostMapping("/areaList")
    public R areaList(){
    	if (aes == null) {
    		aes = eliteService.queryArea();
    	}
        return R.ok().put("data", aes);
    }
    
    
    /**
     * 大区经理列表
     */
    @PostMapping("/eliteList")
    public R eliteList(@RequestParam(name="areaId",required=true) Long areaId){
    	List<EliteEntity> list = eliteService.queryAreaId(areaId);

        return R.ok().put("data", list);
    }
    
    @RequestMapping("voteCaptcha.jpg")
	public void voteCaptcha(HttpServletResponse response,HttpServletRequest request)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        IpValidate.VALIDATE.put(IPUtils.getIpAddr(request)+"code", text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}
    
    
    /**
     * 投票
     */
    @RateLimit
    @PostMapping("/voteElite")
    public R voteElite(VoteDto voteDto,HttpServletRequest request){
    	if (null == voteDto || voteDto.getId() == null) {
    		return R.error().put("msg", "参数错误非法请求哟");
    	}
    	String ip = IPUtils.getIpAddr(request);
    	String svrCode = IpValidate.VALIDATE.get(ip+"code");
    	logger.info(ip+"-IPCode:"+svrCode+"-submitCode-"+voteDto.getValidateCode());
    	if (StringUtils.isBlank(voteDto.getValidateCode()) || voteDto.getValidateCode().length() < 5) {
    		return R.error().put("msg", "验证码不能为空");
    	}
    	if(!StringUtils.equals(voteDto.getValidateCode(), svrCode) && svrCode != null){
    		return R.error().put("msg", "错误的验证码请求哟");
    	}
    	if (!IpValidate.validateIp(ip)) {
    		return R.error().put("msg", "今天你的投票次数已满-明天才能再投票哟");
    	}
    	
    	eliteService.updateEliteVote(voteDto);
    	EliteEntity ee = eliteService.selectById(voteDto.getId());
    	
        return R.ok().put("data", ee);
    }
}
