package io.renren.modules.finance.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.crab2died.ExcelUtils;
import com.github.crab2died.exceptions.Excel4JException;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.finance.entity.EliteEntity;
import io.renren.modules.finance.service.EliteService;



/**
 * 
 *
 * @date 2018-07-30 09:51:16
 */
@RestController
@RequestMapping("elite")
public class EliteController {
    @Autowired
    private EliteService eliteService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:elite:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = eliteService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 导出
     * @throws Exception 
     * @throws Excel4JException 
     */
    @RequestMapping("/export")
    @RequiresPermissions("sys:elite:export")
    public void export(HttpServletResponse response) throws Excel4JException, Exception{
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("limit", 4000);
        PageUtils page = eliteService.queryPage(params);
        
        response.setHeader("Content-disposition", "elite.xlsx"); 
        response.setContentType("application/vnd.ms-excel");   
        response.setHeader("Content-disposition", "attachment;filename=elite.xlsx");   
        response.setHeader("Pragma", "No-cache"); 
        
        ExcelUtils.getInstance().exportObjects2Excel(page.getList(), EliteEntity.class, true,null, true, response.getOutputStream());
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:elite:info")
    public R info(@PathVariable("id") Integer id){
        EliteEntity elite = eliteService.selectById(id);

        return R.ok().put("elite", elite);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:elite:save")
    public R save(@RequestBody EliteEntity elite){
        eliteService.insert(elite);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update") 
    @RequiresPermissions("sys:elite:update")
    public R update(@RequestBody EliteEntity elite){
        ValidatorUtils.validateEntity(elite);
        eliteService.updateAllColumnById(elite);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:elite:delete")
    public R delete(@RequestBody Integer[] ids){
        eliteService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
