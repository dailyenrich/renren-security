package io.renren.modules.finance.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.finance.dao.AreaDao;
import io.renren.modules.finance.entity.AreaEntity;


@Service("areaService")
public class AreaService extends ServiceImpl<AreaDao, AreaEntity> {

    public PageUtils queryPage(Map<String, Object> params) {
        Page<AreaEntity> page = this.selectPage(
                new Query<AreaEntity>(params).getPage(),
                new EntityWrapper<AreaEntity>()
        );

        return new PageUtils(page);
    }
    
    public List<AreaEntity> queryPage() {

        return this.selectList(new EntityWrapper<AreaEntity>());
    }

}
