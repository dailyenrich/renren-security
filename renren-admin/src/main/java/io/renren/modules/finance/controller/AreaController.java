package io.renren.modules.finance.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.finance.entity.AreaEntity;
import io.renren.modules.finance.service.AreaService;



/**
 * 
 *
 * @date 2018-07-30 09:51:16
 */
@RestController
@RequestMapping("area")
public class AreaController {
    @Autowired
    private AreaService areaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = areaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:area:info")
    public R info(@PathVariable("id") Integer id){
        AreaEntity area = areaService.selectById(id);

        return R.ok().put("area", area);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AreaEntity area){
        areaService.insert(area);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AreaEntity area){
        ValidatorUtils.validateEntity(area);
        areaService.updateAllColumnById(area);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:area:delete")
    public R delete(@RequestBody Integer[] ids){
        areaService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }
    
    public static void main(String[] args) {
		String ss = "ASD45";
		
		System.out.println(ss.length());
	}
}
