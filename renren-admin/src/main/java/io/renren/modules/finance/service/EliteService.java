package io.renren.modules.finance.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import io.renren.common.utils.DateUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.finance.dao.EliteDao;
import io.renren.modules.finance.dto.IpValidate;
import io.renren.modules.finance.dto.VoteDto;
import io.renren.modules.finance.entity.AreaEntity;
import io.renren.modules.finance.entity.EliteEntity;


@Service("eliteService")
public class EliteService extends ServiceImpl<EliteDao, EliteEntity>  {
	 
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	EliteDao eliteDao;
	
    public PageUtils queryPage(Map<String, Object> params) {
        Page<EliteEntity> page = this.selectPage(
                new Query<EliteEntity>(params).getPage(),
                new EntityWrapper<EliteEntity>()
        );

        return new PageUtils(page);
    }
    
    public List<EliteEntity> queryAreaId(Long areaId) {
    	Map<String, Object> params = new HashMap<>();
    	params.put("area_id", areaId);
        return this.eliteDao.queryElite(params);
    }
    
    public List<AreaEntity> queryArea() {
        return this.eliteDao.queryArea();
    }

    public int updateEliteVote(VoteDto voteDto) {
    	return eliteDao.updateEliteVote(voteDto);
    }
    
    @Scheduled(cron="0 00 00 ? * *")
    public void clearVotes() {
    	Calendar now = Calendar.getInstance();
    	now.set(Calendar.DATE, now.get(Calendar.DATE)-1);
    	logger.info("删除"+DateUtils.format(now.getTime())+"的日期数据");
    	IpValidate.VOTES_TEMP.remove(DateUtils.format(now.getTime()));
    }
}
