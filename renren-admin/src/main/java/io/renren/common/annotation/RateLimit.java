package io.renren.common.annotation;

import java.lang.annotation.*;

@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimit {
	double limitNum() default 50;  //默认每秒放入桶中的token
}
