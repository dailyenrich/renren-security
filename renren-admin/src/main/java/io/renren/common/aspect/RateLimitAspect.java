package io.renren.common.aspect;


import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.util.concurrent.RateLimiter;

import io.renren.common.annotation.RateLimit;
import io.renren.common.utils.R;

@Component
@Scope
@Aspect
public class RateLimitAspect {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	//用来存放不同接口的RateLimiter(key为接口名称，value为RateLimiter)
    private ConcurrentHashMap<String, RateLimiter> map = new ConcurrentHashMap<>();
    
    private static ObjectMapper objectMapper = new ObjectMapper();
    
    private RateLimiter rateLimiter;
    
    @Autowired
    private HttpServletResponse response;

    
    @Pointcut("@annotation(io.renren.common.annotation.RateLimit)")
    public void serviceLimit() {
    }
    
    @Around("serviceLimit()")
    public Object around(ProceedingJoinPoint joinPoint) throws NoSuchMethodException {
    	
    	 Object obj=null;
    	 //获取拦截的方法名
    	 MethodSignature signature =(MethodSignature) joinPoint.getSignature();
    	 Method method = signature.getMethod();
    	 RateLimit annotation  = method.getAnnotation(RateLimit.class);
    	 
    	 double limitNum = annotation.limitNum();//获取注解每秒加入桶中的token
    	 String methodName=method.getName();//注解所在方法名区分不同的限流策略
    	 
    	 
    	 if(map.containsKey(methodName)){
    		 rateLimiter = map.get(methodName);
    	 }else{
    		 map.put(methodName, rateLimiter.create(limitNum));
    		 rateLimiter = map.get(methodName);
    	 }
    	 
    	 try{
    		 
    		 if(rateLimiter.tryAcquire()){
        		 obj = joinPoint.proceed();
        	 }else{
        		 //拒绝请求(服务降级)
        		 String result = objectMapper.writeValueAsString(R.error("系统繁忙,请重试"));
        		 logger.info("拒绝了请求：" + result);
        		 outErrorResult(result);
        	 }
    		 
    	 }catch(Throwable throwable){
    		 throwable.printStackTrace();
    	 }
    	 return obj;
    }
    
    //将结果返回
    public void outErrorResult(String result) {
        response.setContentType("application/json;charset=UTF-8");
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            outputStream.write(result.getBytes("utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    static {
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

}
